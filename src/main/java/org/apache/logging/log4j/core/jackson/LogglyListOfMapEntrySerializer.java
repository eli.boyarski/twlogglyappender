/**
 * Copyright 2017 Thinkwrap Commerce Inc.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.logging.log4j.core.jackson;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Map;

/**
 * This class serializes the map object into key => value pairs instead of an array of key => values
 */
public class LogglyListOfMapEntrySerializer extends StdSerializer<Map<String, String>> {
    private static final long serialVersionUID = 1L;

    protected LogglyListOfMapEntrySerializer() {
        super(Map.class, false);
    }

    public void serialize(Map<String, String> map, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        jgen.writeStartObject();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            jgen.writeStringField(entry.getKey(), entry.getValue());
        }
        jgen.writeEndObject();
    }
}

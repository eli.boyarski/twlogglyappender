/**
 * Copyright 2017 Thinkwrap Commerce Inc.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.logging.log4j.core.layout;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.jackson.LogStashLog4jJsonObjectMapper;

import java.util.HashSet;
import java.util.Set;

/**
 * Cribbed from log4j core JacksonFactory, this merely introduces LogStashLog4jJsonObjectMapper
 */
abstract class LogStashJacksonFactory extends JacksonFactory {
    /**
     * JSON class
     */
    static class JSON extends JacksonFactory.JSON {
        @Override
        protected ObjectMapper newObjectMapper() {
            return new LogStashLog4jJsonObjectMapper();
        }

        ObjectWriter newWriter(final boolean locationInfo, final boolean properties, final boolean compact) {
            final SimpleFilterProvider filters = new SimpleFilterProvider();
            final Set<String> except = new HashSet<String>(2);
            if (!locationInfo) {
                except.add(this.getPropertNameForSource());
            }
            if (!properties) {
                except.add(this.getPropertNameForContextMap());
            }
            filters.addFilter(Log4jLogEvent.class.getName(), SimpleBeanPropertyFilter.serializeAllExcept(except));
            final ObjectWriter writer = this.newObjectMapper().writer(compact ? this.newCompactPrinter() : this.newPrettyPrinter());
            return writer.with(filters);
        }

        protected String getPropertNameForContextMap() {
            return "contextMap";
        }

    }
}

/**
 * Copyright 2017 Thinkwrap Commerce Inc.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.logging.log4j.core.appender;

import org.apache.log4j.helpers.LogLog;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * LogglyAppender plugin
 *
 * @author carlos.belizon
 */
@Plugin(name = "LogglyAppender", category = "Core", elementType = "appender", printObject = true)
public final class LogglyAppender extends AbstractAppender {

    private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
    private final Lock readLock = rwLock.readLock();
    private LogglyAppender.HttpPost poster;
    private EmbeddedDb db;
    private Object waitLock = new Object();
    private int batchSize = 50;
    private String logglyUrl;
    private String proxyHost = null;
    private int proxyPort = -1;

    protected LogglyAppender(String name, Filter filter, Layout<? extends Serializable> layout, final boolean ignoreExceptions, String dirName, String url, int batchSize, String proxyHost,
            int proxyPort) {
        super(name, filter, layout, ignoreExceptions);
        this.logglyUrl = url;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.batchSize = batchSize;

        LogLog.debug("Creating database in " + dirName + " with name " + getName());
        this.db = new EmbeddedDb(dirName, getName(), null);

        this.poster = new LogglyAppender.HttpPost();

        Thread posterThread = new Thread(poster);
        posterThread.start();

    }

    // The append method is where the appender does the work.
    // Given a log event, you are free to do with it what you want.
    // This example demonstrates:
    // 1. Concurrency: this method may be called by multiple threads concurrently
    // 2. How to use layouts
    // 3. Error handling
    @Override
    public void append(LogEvent event) {
        /**
         * We always only produce to the current file. So there's no need for locking
         */

        assert this.getLayout() != null : "Cannot log, there is no layout configured.";

        String output = (String) this.getLayout().toSerializable(event);

        synchronized (waitLock) {
            db.writeEntry(output, System.nanoTime());
            waitLock.notify();
        }

        if (poster.getState() == LogglyAppender.ThreadState.STOPPED) {
            LogLog.debug("Noticed thread stopped!");
        }
    }

    /**
     * Your custom appender needs to declare a factory method
     * annotated with `@PluginFactory`. Log4j will parse the configuration
     * and call this factory method to construct an appender instance with
     * the configured attributes.
     *
     * @param name
     * @param layout
     * @param filter
     * @param dirName
     * @param url
     * @param batchSize
     * @param proxyHost
     * @param proxyPort
     * @return
     */
    @PluginFactory
    public static LogglyAppender createAppender(@PluginAttribute("name") String name, @PluginElement("Layout") Layout<? extends Serializable> layout, @PluginElement("Filter") final Filter filter, @PluginAttribute("dirName") String dirName, @PluginAttribute("url") String url, @PluginAttribute("batchSize") int batchSize, @PluginAttribute("proxyHost") String proxyHost,
            @PluginAttribute("proxyPort") int proxyPort) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        if (url == null) {
            LOGGER.error("No url for MyCustomAppenderImpl");
            return null;
        }
        return new LogglyAppender(name, filter, layout, true, dirName, url, batchSize, proxyHost, proxyPort);
    }

    /**
     * ThreadState enum
     */
    private enum ThreadState {
        START,
        RUNNING,
        STOP_REQUESTED,
        STOPPED
    }

    /**
     * Http post class
     */
    private class HttpPost implements Runnable {

        // State variables needs to be volatile, otherwise it can be cached local to the thread and stop() will never work
        private volatile LogglyAppender.ThreadState curState = LogglyAppender.ThreadState.START;
        private volatile LogglyAppender.ThreadState requestedState = LogglyAppender.ThreadState.RUNNING;
        private final Object stopLock = new Object();

        public void run() {

            curState = LogglyAppender.ThreadState.RUNNING;
            LogLog.debug("Loggly: background thread waiting for db");
            boolean initialized = waitUntilDbInitialized();
            if (initialized) {
                LogLog.debug("Loggly: background thread starting");
                List<Entry> messages = db.getNext(batchSize);
                // ThreadState.STOP_REQUESTED lets us keep running until our queue is empty, but stop when it is
                while (curState == LogglyAppender.ThreadState.RUNNING || (curState == LogglyAppender.ThreadState.STOP_REQUESTED && messages != null && messages.size() > 0)) {
                    if (curState == LogglyAppender.ThreadState.STOP_REQUESTED) {
                        LogLog.warn("Loggly: Stop requested, emptying queue of: " + messages.size());
                    }

                    if (messages == null || messages.size() == 0) {
                        // We aren't synchronized around the database, because that doesn't matter
                        // this synchronization block just lets us be notified sooner if a new message comes it
                        synchronized (waitLock) {
                            try {
                                // nothing to consume, sleep for 1 second
                                waitLock.wait(1000);
                            } catch (InterruptedException e) {
                                if (curState == LogglyAppender.ThreadState.STOP_REQUESTED) {
                                    LogLog.debug("Stopping thread");
                                } else {
                                    // an error
                                    LogLog.error("Unable to sleep for 1 second in queue consumer");
                                }
                            }
                        }
                    } else {

                        try {
                            int response = sendData(messages);
                            switch (response) {
                            case 200:
                            case 201: {
                                db.deleteEntries(messages);
                                break;
                            }
                            case 400: {
                                LogLog.warn("loggly: bad request dumping message");
                                db.deleteEntries(messages);
                            }
                            default: {
                                LogLog.error("Received error code " + response + " from Loggly servers.");
                            }
                            }
                        } catch (IOException e) {
                            LogLog.error(String.format("Unable to send data to loggly at URL %s", logglyUrl));
                        }
                    }

                    // The order of these two if statements (and the else) is very important
                    // If the order was reversed, we would drop straight from RUNNING to STOPPED without one last 'cleanup' pass.
                    // If the else was missing, we would permently be stuck in the STOP_REQUESTED state.
                    if (curState == LogglyAppender.ThreadState.STOP_REQUESTED) {
                        curState = LogglyAppender.ThreadState.STOPPED;
                    } else if (requestedState == LogglyAppender.ThreadState.STOPPED) {
                        curState = LogglyAppender.ThreadState.STOP_REQUESTED;
                    }
                    messages = db.getNext(batchSize);
                }
                LogLog.debug("Loggly background thread is stopped.");
            } else {
                LogLog.warn("Loggly bailing out because we were interrupted while waiting to initialize");
                curState = LogglyAppender.ThreadState.STOPPED;
            }

            synchronized (stopLock) {
                stopLock.notify();
            }
        }

        /**
         * @return
         */
        public LogglyAppender.ThreadState getState() {
            return curState;
        }

        /**
         * Waits until the db is initialized, or stop has been requested.
         *
         * @return
         */
        public boolean waitUntilDbInitialized() {

            synchronized (db.getInitializeLock()) {
                while (!db.isInitialized() && requestedState != LogglyAppender.ThreadState.STOPPED) {
                    try {
                        db.getInitializeLock().wait();
                    } catch (InterruptedException e) {
                        LogLog.error("Loggly Appender interrupted waiting for db initalization", e);
                    }
                }
            }

            // if this returns false, we should abort, because it means we were interrupted
            // after shutdown was requested.
            return db.isInitialized();
        }

        /**
         * Send the data via http post
         *
         * @param messages
         * @throws IOException
         */

        private int sendData(List<Entry> messages) throws IOException {
            URL url = new URL(logglyUrl);
            Proxy proxy = Proxy.NO_PROXY;
            if (proxyHost != null) {
                SocketAddress addr = new InetSocketAddress(proxyHost, proxyPort);
                proxy = new Proxy(Proxy.Type.HTTP, addr);
            }

            URLConnection conn = url.openConnection(proxy);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            // conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Type", "text/plain");
            OutputStream os = conn.getOutputStream();

            for (Entry message : messages) {
                final byte[] msgBytes = message.getMessage().getBytes();
                conn.getOutputStream().write(msgBytes);
            }

            os.flush();
            os.close();
            HttpURLConnection huc = ((HttpURLConnection) conn);
            int respCode = huc.getResponseCode();
            // grabbed from http://download.oracle.com/javase/1.5.0/docs/guide/net/http-keepalive.html
            BufferedReader in = null;
            StringBuffer response = null;
            try {
                in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                response = new StringBuffer();
                int value = -1;
                while ((value = in.read()) != -1) {
                    response.append((char) value);
                }
                in.close();
            } catch (IOException e) {
                try {
                    response = new StringBuffer();
                    response.append("Status: ").append(respCode).append(" body: ");
                    in = new BufferedReader(new InputStreamReader(huc.getErrorStream()));
                    int value = -1;
                    while ((value = in.read()) != -1) {
                        response.append((char) value);
                    }
                    in.close();
                    LogLog.error(String.format("Unable to send data to loggly at URL %s Response %s", logglyUrl, response));
                } catch (IOException ee) {
                    LogLog.error(String.format("Unable to send data to loggly at URL %s", logglyUrl));
                }
            }
            return respCode;
        }

        /**
         * Stop this thread sending data and write the last read position
         */
        public void stop() {
            LogLog.debug("Stopping background thread");
            requestedState = LogglyAppender.ThreadState.STOPPED;

            // Poke the thread to shut it down.
            synchronized (waitLock) {
                LogLog.debug("Loggly: Waking background thread up");
                waitLock.notify();
            }

            synchronized (poster.stopLock) {
                LogLog.debug("Loggly: Waiting for background thread to stop");
                while (poster.curState != LogglyAppender.ThreadState.STOPPED) {
                    try {
                        poster.stopLock.wait(100);
                    } catch (InterruptedException e) {
                        LogLog.error("Interrupted while waiting for Http thread to stop, bailing out.");
                    }
                }
            }
        }

    }
}
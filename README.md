# Introduction #
This package contains the necessary plugins to send as json format your log lines to Loggly

# How to build it? #
In your shell command line execute:

```
mvn clean install
```

# How to wire it up in your project? #

Put the following properties in your properties file:

```
log4j2.filters=threshold
log4j2.filter.threshold.type=ThresholdFilter
log4j2.filter.threshold.level=info
log4j2.rootLogger.level=info
log4j2.appenders=loggly
log4j2.appender.loggly.type=LogglyAppender
log4j2.appender.loggly.name=LogglyAppenderDebugLog
log4j2.appender.loggly.layout.type=LogStashJSONLayout
log4j2.appender.loggly.layout.userFields=env:${environment},nodeId:${nodeId}
log4j2.appender.loggly.layout.properties=true
log4j2.appender.loggly.proxyPort=8080
log4j2.appender.loggly.batchSize=40
log4j2.appender.loggly.dirName=${LOGS_DIR}/loggly
log4j2.appender.loggly.filters=threshold
log4j2.appender.loggly.filter.threshold.type=ThresholdFilter
log4j2.appender.loggly.filter.threshold.level=debug
log4j2.rootLogger.appenderRefs=stdout,loggly
log4j2.rootLogger.appenderRef.loggly.ref=LogglyAppenderDebugLog
log4j2.appender.loggly.url=http://logs-01.loggly.com/inputs/${your_token}/tag/${your_tag}

```



Visit us at www.thinkwrap.com

Contact us at www.thinkwrap.com/contact 



MIT License

Copyright (c) 2017 Thinkwrap Commerce Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.